<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Educ-canine',
    'description' => 'Configuration du site Educ-canine',
    'category' => 'distribution',
    'version' => '1.0.0',
    'state' => 'beta',
    'clearcacheonload' => 1,
    'author' => 'Thomas Beck',
    'author_email' => 'contact@thomas-beck.fr',
    'author_company' => '',
    'constraints' => array(
        'depends' => array(
            'typo3' => '10.0.0-11.99.99',
        ),
        'suggests' => array(
        ),
        'conflicts' => array(
            'templavoila' => ''
        ),
    ),
    'autoload' => array(
        'psr-4' => array(
            'Tbeck\\MyEduccanine\\' => 'Classes'
        )
    )
);
