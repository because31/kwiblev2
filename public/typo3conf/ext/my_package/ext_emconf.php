<?php
$EM_CONF[$_EXTKEY] = array(
    'title' => 'Configuration de base',
    'description' => '',
    'category' => 'distribution',
    'version' => '1.0.0',
    'state' => 'beta',
    'clearcacheonload' => 1,
    'author' => 'Thomas Beck',
    'author_email' => 'contact@thomas-beck.fr',
    'author_company' => '',
);
