<?php
defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'my_package',
    'Configuration/TSconfig/config.tsconfig',
    'EXT:my_package :: Configuration de base');

    