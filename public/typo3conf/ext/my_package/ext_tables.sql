CREATE TABLE tt_content (
    toggle_content int(11) DEFAULT '0' NOT NULL,
    toggle_position int(11) DEFAULT '0' NOT NULL,
    background_image int(11) DEFAULT '0' NOT NULL,
    container_fluid int(11) DEFAULT '0' NOT NULL,
);