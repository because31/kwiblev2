<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['myportfolio'] = 'EXT:my_portfolio/Configuration/RTE/rte.yaml';

// Ajouter des couleurs dans l'arborescence backend.
/*
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('
        options.pageTree.backgroundColor.uidPage = rgba(255,109,0,0.2)
');
*/
