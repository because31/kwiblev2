<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'my_portfolio',
    'Configuration/TypoScript',
    'Configuration du site Thomas Beck'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'my_portfolio',
    'Configuration/TSConfig/tsconfig.txt',
    'EXT:my_portfolio : Thomas Beck - Tsconfig'
);

