const gulp = require('gulp'),
    sass = require('gulp-sass'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCSS = require('gulp-clean-css'),
    del = require('del'),
    svgSprite = require('gulp-svg-sprite'),
    bs = require('browser-sync').create();

// const publicPath = 'public/typo3conf/ext/my_portfolio/Resources/Public';
  const publicPath = 'public/typo3conf/ext/my_educcanine/Resources/Public';
// const publicPath = 'decoupageEducCanine';

const paths = {
    scss: publicPath + '/Scss',
    js: publicPath + '/JavaScript',
    sprites: publicPath + '/Icons',
    dist: {
        css: publicPath + '/dist/Css',
        js: publicPath + '/dist/JavaScript',
        sprites: publicPath + '/dist/Images',
    }
};

// Clean web assets
function clean() {
    return del([
        paths.dist.css,
        paths.scss + '/sprites/**/*',
        paths.dist.sprites,
        paths.dist.js
    ]);
}

// Sprites SVG
var config = {
    mode: {
      symbol: { // symbol mode to build the SVG
        render: {
          css: false, // CSS output option for icon sizing
          scss: false // SCSS output option for icon sizing
        },
        dest: '', // destination foldeer
        prefix: '.svg--%s', // BEM-style prefix if styles rendered
        sprite: 'sprite.svg', //sprite name
        example: true // Build sample page
      }
    },
    svg: {
      xmlDeclaration: false, // strip out the XML attribute
      doctypeDeclaration: false // don't include the !DOCTYPE declaration
    }
  };
function spriteSvg() {
    return gulp.src( paths.sprites+'/**/*.svg')
        .pipe(svgSprite(config))
        .pipe(gulp.dest(paths.dist.sprites));
}

// Styles
function mainSass() {
    return gulp.src(paths.scss + '/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cleanCSS({keepSpecialComments : 0}))
        .pipe(rename({basename:'main', suffix: '.min'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.dist.css))
        .pipe(bs.stream({match: '**/*.css'}));
}
// RTE
function rteSass() {
    return gulp.src(paths.scss + '/rte.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cleanCSS({keepSpecialComments : 0}))
        .pipe(rename({basename:'rte', suffix: '.min'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.dist.css))
        .pipe(bs.stream({match: '**/*.css'}));
}
// VENDOR
function vendorCss() {
    return gulp.src([
	    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css',
        'node_modules/animate.css/animate.min.css'
    ])
    .pipe(concat('vendor.min.css'))
    .pipe(gulp.dest(paths.dist.cssPackage))
}

// Concatenate JS scripts
function js() {
    return gulp.src([
	    paths.js + '/*.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(concat('scripts.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.dist.js))
        .pipe(bs.stream({match: '**/*.js'}));
}


// Concatenate vendor files
function vendorJs() {
    return gulp.src([
        'node_modules/@popperjs/core/dist/umd/popper.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
        'node_modules/hover-dir/dist/hoverDir.min.js',
        'node_modules/tarteaucitronjs/tarteaucitron.js'
    ])
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest(paths.dist.js))
}

// Start BrowserSync server
function browserSync(done) {
    browsersync.init({
      open: false,
      proxy: 'nginx:80',
      //notify: false,
    });
    done();
  }
  // BrowserSync Reload
  function browserSyncReload(done) {
    browsersync.reload();
    done();
  }

// Watch changes
function watchFiles() {
  gulp.watch(paths.scss + '/**/*.scss', { usePolling: true }, mainSass);
  gulp.watch(paths.js + '/*.js', { usePolling: true }, js);
  gulp.watch(paths.sprites+'/**/*.svg', { usePolling: true }, spriteSvg);
}

// define complex tasks
const build = gulp.series(clean, gulp.parallel(mainSass, spriteSvg, js, vendorJs));
const watch = gulp.parallel(watchFiles);

exports.clean = clean;
exports.spriteSvg = spriteSvg;
exports.mainSass = mainSass;
// exports.vendorCss = vendorCss;
exports.js = js;
exports.vendorJs = vendorJs;
exports.build = build;
exports.watch = watch;
exports.default = build;
// exports.rteSass = rteSass;
